const Express = require("express");
const BodyParser = require("body-parser");
const Mongo = require('mongodb');
const MongoClient = require("mongodb").MongoClient;
const ObjectId = require("mongodb").ObjectID;
const Grid = require('gridfs-stream');

const CONNECTION_URL = "mongodb+srv://team8admin:team8ser402@cluster0-ffvo3.mongodb.net/admin?retryWrites=true";
const DATABASE_NAME = "studyhelpar";

var app = Express();

app.use(BodyParser.json());
app.use(BodyParser.urlencoded({ extended: true }));

var database, collection, imageCollection;
var gfs;

require('dotenv').config();

app.listen(process.env.PORT || 3000, () => {
	//console.log(process.env);
	console.log("Connected to port = " + process.env.port);
    MongoClient.connect(CONNECTION_URL, { useNewUrlParser: true }, (error, client) => {
        if(error) {
            throw error;
        }
        database = client.db(DATABASE_NAME);
        collection = database.collection("textbook");
        imageCollection = database.collection("fs");
        console.log("Connected to database: `" + DATABASE_NAME + "`!");

        gfs = Grid(database,Mongo);
    });
});

app.get("/textbooks", (request, response) => {
    collection.find({}).toArray((error, result) => {
        if(error) {
            return response.status(500).send(error);
        }
        response.send(result);
    });
});

app.get("textbooks/:id", (request, response) => {
    collection.findOne({ "_id": new ObjectId(request.params.id) }, (error, result) => {
        if(error) {
            return response.status(500).send(error);
        }
        response.send(result);
    });
});

app.get("textbooks/image/:id", (request, response) => {
    var readstream = gfs.createReadStream({
    	_id: request.params.id
    });

    readstream.pipe(response);
});

app.post("/textbook", (request, response) => {
    collection.insert(request.body, (error, result) => {
        if(error) {
            return response.status(500).send(error);
        }
        response.send(result.result);
    });
});

app.put("/person/:id", async (request, response) => {
    try {
        var person = await collection.findById(request.params.id).exec();
        collection.set(request.body);
        var result = await collection.save();
        response.send(result);
    } catch (error) {
        response.status(500).send(error);
    }
});

app.delete("/textbook/:id", async (request, response) => {
    try {
        var result = await collection.deleteOne({ _id: request.params.id }).exec();
        response.send(result);
    } catch (error) {
        response.status(500).send(error);
    }
});