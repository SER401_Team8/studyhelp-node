var mongoose = require('mongoose');

var Schema = mongoose.Schema;

var photoSchema = new Schema({
  name:  { type: String },
  code:  { type: String },
  desc:  { type: String },
  imageId: { type: String }
  });

module.exports = mongoose.model('textbook', photoSchema);