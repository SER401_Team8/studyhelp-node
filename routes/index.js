var express = require('express');
var router    = express.Router();
var upload    = require('./upload');
var mongoose  = require('mongoose');
var Textbook  = mongoose.model('textbook');
var base64Img = require('base64-img');
var cors = require('cors');
var fs = require('fs');

router.use(cors());
var bodyParser = require('body-parser')
router.use( bodyParser.json() );       // to support JSON-encoded bodies
router.use(bodyParser.urlencoded({     // to support URL-encoded bodies
  limit: "15360mb",
  extended: true
})); 

/* GET home page. */
router.get('/', function(req, res, next) {

    //Textbook.find({}, ['name','code','desc','imageId'], {sort:{ _id: -1} }, function(err, photos) {
    Textbook.find({}, function(err, photos) {
    //Textbook.find({}, function(err, photos) {
    //console.log('in home page ' + photos.length);
    res.render('index', { title: 'NodeJS file upload tutorial', msg:req.query.msg, photolist : photos });
    
  });

});

//====GET ALL SIGNATURES===//
router.get('/photos', function(req, res, next) {
  console.log(Textbook);
  Textbook.find({}, ['path','caption'], {sort:{ _id: -1} }, function(err, photos) {
      res.send(photos);
  });

});
//==========================//

router.post('/upload', function(req, res) {
  base64Img.img(req.body.file, './public/files', req.body.comment, function(err, filepath) {});
  upload(req, res,(error) => {
      if(error){
         res.redirect('/?msg=3');
      }else{
            /**
             * Create new record in mongoDB
             */
            var fullPath = "files/"+ req.body.textbook+ "/"+req.body.comment;
            console.log(req.body.comment)
            var document = {
              path:     fullPath, 
              caption:   req.body.comment
            };
  
          var photo = new Textbook(document); 
          photo.save(function(error){
            if(error){ 
              throw error;
            }
            res.redirect('/?msg=1');
         });
      }
  });    
});

//For obj files
router.post('/objupload', function(req, res) {

  let data = req.body.file;
  if(req.body.second == false){
    fs.writeFile('./public/files'+req.body.comment+".sfb", data, (err) => { 
      if (err) res.redirect('/?msg=3');
    });
    res.redirect('/?msg=1')
  }
  else if(req.body.second == true){
    fs.appendFile('./public/files'+req.body.comment+".sfb", data, (err) => { 
      if (err) res.redirect('/?msg=3');
    });
    res.redirect('/?msg=1')
  }
  else{
    res.redirect('/?msg=3');
  }
  
    
    
});
module.exports = router;
